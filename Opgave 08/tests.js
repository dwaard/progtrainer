var tests = [
//  [  a[], exp, name]
	[  [1,2,3], 2, 'Kleine reeks, met geheel getal als uitkomst' ],
	[  [0, 0, 0, 0, 0, 0, 0], 0, 'Een reeks nullen'],
	[  [3, 4, 3], 10/3, 'Kleine reeks, gebroken getal als uitkomst'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	exp = testdata[1];
	name = testdata[2];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a);		
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result == exp) {
		return false;
	}	

	// Create an alert div with the result 
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	// Create an alert div with the result 
	var resultTxt = "functionUnderTest(" + a + ") -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}
