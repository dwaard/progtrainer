var tests = [
//  [  a, exp]
	[    0,  32],
	[  100, 212],
	[  -10,  14],
	[  115, 239],
	[  170, 338],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	exp = testdata[1];
	name = "" + a + "C moet " + exp + "F worden";
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a);		
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result == exp) {
		return false;
	}	

	// Create an alert div with the result 
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	// Create an alert div with the result 
	var resultTxt = "functionUnderTest(" + a + ") -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', resultTxt);
	return true;
}


