var tests = [
//  [ a, exp]
	[   0, '00000000'],
	[   1, '00000001'],
	[   2, '00000010'],
	[   4, '00000100'],
	[   8, '00001000'],
	[  16, '00010000'],
	[  32, '00100000'],
	[  64, '01000000'],
	[ 128, '10000000'],
	[ 129, '10000001'],
	[ 130, '10000010'],
	[ 132, '10000100'],
	[ 136, '10001000'],
	[ 144, '10010000'],
	[ 160, '10100000'],
	[ 192, '11000000'],
	[ 255, '11111111'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	exp = testdata[1];
	name = '' + a + ' wordt ' + exp;
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a);
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// Test postcondities, onder andere of er iets is teruggegeven
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	var type = typeof result;
	if (!(type=='string')) {
		appendAlert('danger', result, 'De waarde in het return statement is een ' + type +' en geen string! Gebruik de "" tekens.');
		return true;
	}

	var length = result.length;
	if (length!=8) {
		appendAlert('danger', result, 'De string in het return statement is ' + length +' tekens lang. Het moet 8 zijn!');
		return true;
	}


	// return TRUE when result equals expected
	if (arraysEqual(result,exp)) {
		return false;
	} else {
		var resultTxt = "functionUnderTest(" + a + ") -> " + result + " (verwacht resultaat: " + exp + ")";
		appendAlert('danger', name, resultTxt);
		return true;
	}
}


function appendAlert(type, strong, msg) {
	var resultTxt = '<div class="alert alert-' + type + '" role="alert"><strong>' + strong + "</strong> " + msg + '</div>';
	$('#result').append(resultTxt);
}
