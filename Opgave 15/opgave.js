/*
 * Maak deze functie zo dat het getal in de parameter a wordt omgezet in string met
 * daarin een binaire code.
 * Voor een uitleg over binaire getallen: https://www.beterrekenen.nl/website/index.php?pag=259
 * De parameter a is een getal van maximaal 1 byte (dus maximaal ???). De return-waarde
 * is een string die bestaat uit een reeks van exact 8 nullen en enen.
 * Voorbeeld: a=5 levert '00000101'
 */

function functionUnderTest(a) {
	// Programmeer hier je oplossing. Gebruik het return statement om iets 'terug te geven'.
	
	return ;
}
