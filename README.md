# LEESMIJ #

Programmeer oefeningen met javascript.

### Waar is deze repository voor? ###
Als je wat programmeerervaring wil opdoen, kun je deze code gebruiken.

### Hoe ga ik ermee aan de slag? ###

* Clone dit project ergens op je computer
* Blader met je bestandsverkenner naar één van de opgaven in dit project
* Open het bestand opgave.html met je browser
* Gebruik een tekst-editor om het bestand opgave.js te bewerken
* Ververs de pagina als je je code wil testen.
* Als je de opgave goed hebt uitgewerkt, verschijnt er een groene balk ipv de rode.
* Have Fun

O ja, vergeet niet dat opgave 1 de makkelijkste is...