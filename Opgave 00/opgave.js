/*
 * Maak deze functie zo dat de tekst "Hello World!" teruggegeven wordt
 */
function functionUnderTest() {
	// Programmeer hier je oplossing. Gebruik het return statement om iets 'terug te geven'.
	return "Hello World!";
}
