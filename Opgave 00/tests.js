var tests = [
//  [ exp, name]
	[  'Hello World!', 'Test hello world'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	exp = testdata[0];
	name = testdata[1];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest();		
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result===exp) {
		return false;
	}	

	// Create an alert div with the result 
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	if (!((typeof result)=='string')) {
		appendAlert('danger', result, 'De waarde in het return statement is geen string! Gebruik de "" tekens.');
		return true;
	}

	if (result.length == 0) {
		appendAlert('danger', result, 'Je hebt een lege string teruggegeven!');
		return true;
	}

	if (result.length != exp.length) {
		appendAlert('danger', result, "String heeft niet de juiste lengte.");
		if (!result.endsWith('!'))
			appendAlert('danger', result, "Je bent het !-teken aan het einde vergeten.");
		return true;
	}

	if (result.toLowerCase() == exp.toLowerCase()) {
		appendAlert('danger', result, "Je bent hoofdletters vergeten.");
		return true;
	}

	var resultTxt = "functionUnderTest() -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}


function appendAlert(type, strong, msg) {
	var resultTxt = '<div class="alert alert-' + type + '" role="alert"><strong>' + strong + "</strong> " + msg + '</div>';
	$('#result').append(resultTxt);	
}