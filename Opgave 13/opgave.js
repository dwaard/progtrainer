/*
 * Maak deze functie zo dat de in a meegegeven array sorteert op AFLOPENDE Volgorde.
 * de parameter a bevat een array van getallen. De grootte daarvan kan variëren (kan ook nul zijn).
 * Programmeer je uitwerking zo dat de elementen in de array worden verplaatst zodat ze in de
 * juiste volgorde komen te staan.
 *
 * Dit is een oefening in loops, dus je mag hiervoor de .sort() function NIET gebruiken.
 * Een bekende manier om dit op te lossen is BubbleSort (https://nl.wikipedia.org/wiki/Bubblesort)
 * Probeer de oplossing zelf te programmeren aan de hand van de in de wiki beschreven werking,
 * in plaats van een kant-en-klaar javascript voorbeeld te kopiëren of de code uit de voorbeelden in
 * andere talen proberen over te typen.
 */
function functionUnderTest(a) {
	// Programmeer hier je oplossing. Gebruik het return statement om iets 'terug te geven'.

	// Om de tests goed te laten verlopen moet dit blijven staan:
	return ;
}
