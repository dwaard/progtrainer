var ta1 = [9,87,99,5,3,8,5,1,0,12,65,145543,-1,7];

var tests = [
//  [ exp, name]
		[  [], [], 'Test lege array'],
		[  [1], [1], 'Test array met één element'],
		[  [1,2], [1,2], 'Test array met twee elementen'],
		[  [2,1], [1,2], 'Test array met twee elementen, in verkeerde volgorde'],
		[  [1,1], [1,1], 'Test array met twee gelijke elementen'],
		[  [1,2,3], [1,2,3], 'Test alle varianten van [1,2,3], stap 1'],
		[  [1,3,2], [1,2,3], 'Test alle varianten van [1,2,3], stap 2'],
		[  [2,1,3], [1,2,3], 'Test alle varianten van [1,2,3], stap 3'],
		[  [2,3,1], [1,2,3], 'Test alle varianten van [1,2,3], stap 4'],
		[  [3,1,2], [1,2,3], 'Test alle varianten van [1,2,3], stap 5'],
		[  [3,2,1], [1,2,3], 'Test alle varianten van [1,2,3], stap 6'],
		[  ta1, ta1.slice().sort(function(a,b){return a-b}), 'Test array met redelijk veel elementen'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	exp = testdata[1];
	name = testdata[2];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a.slice());
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// Test postcondities, onder andere of er iets is teruggegeven
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	if (!((typeof result)=='object')) {
		appendAlert('danger', result, 'De waarde in het return statement is een ' + (typeof result ) +'! Gebruik de "" tekens.');
		return true;
	}

	if (result.length != exp.length) {
		appendAlert('danger', result, "String heeft niet de juiste lengte.");
		if (!result.endsWith('!'))
			appendAlert('danger', result, "Je bent het !-teken aan het einde vergeten.");
		return true;
	}

	// return TRUE when result equals expected
	if (arraysEqual(result,exp)) {
		return false;
	} else {
		var resultTxt = "functionUnderTest() -> " + result + " (verwacht resultaat: " + exp + ")";
		appendAlert('danger', name, resultTxt);
		return true;
	}

}


function appendAlert(type, strong, msg) {
	var resultTxt = '<div class="alert alert-' + type + '" role="alert"><strong>' + strong + "</strong> " + msg + '</div>';
	$('#result').append(resultTxt);
}
