var tests = [
//  [ a, exp]
	[  0, 'nul'],
	[  1, 'een'],
	[  10, 'tien'],
	[  11, 'elf'],
	[  12, 'twaalf'],
	[  13, 'dertien'],
	[  14, 'veertien'],
	[  15, 'vijftien'],
	[  18, 'achttien'],
	[  20, 'twintig'],
	[  22, 'tweeentwintig'],
	[  47, 'zevenenveertig'],
	[  99, 'negenennegentig'],
	[ 100, 'honderd'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	exp = testdata[1];
	name = '' + a + ' wordt ' + exp;
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a);
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// Test postcondities, onder andere of er iets is teruggegeven
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	if (!((typeof result)=='string')) {
		appendAlert('danger', result, 'De waarde in het return statement is een ' + (typeof result ) +'! Gebruik de "" tekens.');
		return true;
	}

	// return TRUE when result equals expected
	if (arraysEqual(result,exp)) {
		return false;
	} else {
		var resultTxt = "functionUnderTest(" + a + ") -> " + result + " (verwacht resultaat: " + exp + ")";
		appendAlert('danger', name, resultTxt);
		return true;
	}
}


function appendAlert(type, strong, msg) {
	var resultTxt = '<div class="alert alert-' + type + '" role="alert"><strong>' + strong + "</strong> " + msg + '</div>';
	$('#result').append(resultTxt);
}
