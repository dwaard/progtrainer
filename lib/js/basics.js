/*
 * Basic functions that help initializing and testing
 */

/*
 * Helper function to check if two arrays are equal
 *
 * var a - the first array
 * var b - the second array
 *
 * returns true is a and b are not null, both arrays are equal insize and the content of each element in both arrays is equal
 */
function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  // If you don't care about the order of the elements inside
  // the array, you should sort both arrays here.

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}


/*
 * Helper function to append a test result element in the page
 *
 * var type - Bootstrap contextual class (makes the color). It should be either:
 *            - success (greenish)
 *            - info (blueish)
 *            - warning - (yellowish)
 *            - danger (reddish)
 *
 * var strong - the text that will be printed in bold font
 * var msg - the message that will be printed
 */
function appendAlert(type, strong, msg) {
	var resultTxt = '<div class="alert alert-' + type + '" role="alert"><strong>' + strong + "</strong> " + msg + '</div>';
	$('#result').append(resultTxt);
}

/*
 * Reads the tests array and calls the function under test if possible
 */
function testAll() {
  // Check if function exists. If so, it wil call the test function repeatedly with different testdata
  if (typeof functionUnderTest == 'function') {
    var fail = false;
    var arrayLength = tests.length;
    for (var i = 0; i < arrayLength; i++) {
      // call test on each row in tests
      fail = test(tests[i]) || fail;
      if (fail) return;
    }
    if (!fail) {
      appendAlert('success', 'SUCCES', 'Alle tests zijn succesvol doorlopen.');
    }
  } else {
    appendAlert('danger', 'FOUT: Functie bestaat niet!', 'Dit betekent waarschijnlijk dat de code van opgave.js een fout bevat.');
  }
}


/*
 * The 'main' code that initializes the page and runs the test
 */
$(function() {
  // Decode the page title from
  var path = window.location.pathname;
  var pathArr = path.split("/");
  var page = pathArr.pop();
  var folder = pathArr.pop();
  $('.title').html(window.decodeURIComponent(folder));
  testAll();
})
