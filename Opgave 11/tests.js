var tests = [
//[  zon, regen, temp, wind]
  [  true, false, 30, 1,  true, "Prachtig warm terrasweer"],
  [  true, false, 30, 5,  true, "Prachtig warm terrasweer. Hoewel je met die wind beter kunt gaan windsurfen"],
	[ false,  true, 30, 1, false, "Het regent, dus geen terrasweer"],
	[  true,  true, 30, 1, false, "De zon schijnt en het regent. Raar, maar geen terrasweer"],
	[ false, false, 21, 1,  true, "Bewolkt, maar net aan lekker terrasweer"],
	[ false, false, 21, 5,  true, "Bewolkt, maar net aan lekker terrasweer, ondanks de harde wind"],
	[ false, false, 20, 1, false, "Bewolkt, maar net niet warm genoeg voor het terras"],
	[ false, false, 15, 1, false, "Bewolkt en koel. Geen terrasweer"],
	[  true, false, 15, 1,  true, "Zonnetje en koel. Wel terrasweer"],
	[  true, false, 10, 4,  true, "Zonnetje en net niet te koel voor die wind. Wel terrasweer"],
	[  true, false,  9, 4, false, "Zonnetje en wel te koel voor die wind. Niet naar het terras"],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	zon = testdata[0];
	regen = testdata[1];
	temp = testdata[2];
	wind = testdata[3];
	exp = testdata[4];
	name = testdata[5];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(zon, regen, temp, wind);
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result == exp) {
		return false;
	}

	// Create an alert div with the result
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	// Create an alert div with the result
	var resultTxt = "functionUnderTest(" + zon + ", " + regen + ", " + temp + ", " + wind + ") -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}
