/*
 * Case Terras-beslisser. Bepaal of het terrasweer is. Als het terrasweer is
 * geef dan de waarde true (niet de string 'true') terug, anders false.
 * Om te bepalen of het terrasweer is, hanteer je de volgende regels
 *  - Het is GEEN terrasweer als het regent (parameter regen is true)
 *  - Het is altijd terrasweer als de temp groter is dan 20C
 *  - Ook als het kouder is, maar het zonnetje schijnt (zon is true) kunnen we
 *    naar het terras
 *  - Is de temp < 10C dan is het alleen terrasweer als er weinig wind
 *    (wind kleiner dan 2) staat. De zon moet dan ook wel schijnen.
 */
function functionUnderTest(zon, regen, temp, wind) {
	// Programmeer hier je oplossing
	return;
}
