var tests = [
//  [  a, b, c, d, exp,       name]
	[  1, 2, 3, 4, [1,2,3,4], 'Al op volgorde'],
	[  2, 1, 3, 4, [1,2,3,4], 'a en b omgedraaid'],
	[  1, 3, 2, 4, [1,2,3,4], 'b en c omgedraaid'],
	[  1, 2, 4, 3, [1,2,3,4], 'c en d omgedraaid'],
	[  3, 2, 1, 4, [1,2,3,4], 'a en c omgedraaid'],
	[  4, 2, 3, 1, [1,2,3,4], 'a en d omgedraaid'],
	[  2, 1, 3, 4, [1,2,3,4], 'a en b omgedraaid'],
	[  4, 3, 2, 1, [1,2,3,4], 'Helemaal omgedraaid'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	b = testdata[1];
	c = testdata[2];
	d = testdata[3];
	exp = testdata[4];
	name = testdata[5];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a, b, c, d);		
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (arraysEqual(result, exp)) {
		return false;
	}

	// Create an alert div with the result
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	// Create an alert div with the result
	var resultTxt = "functionUnderTest(" + a + ", " + b + ", " + c + ", " + d + ") -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}
