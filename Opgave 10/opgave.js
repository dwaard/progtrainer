/*
 * Maak deze functie zo dat a<=b, b<=c en c<=d (sorteer in oplopende volgorde)
 * De sport is om dit NIET met de .sort() methode op te lossen.
 */
function functionUnderTest(a, b, c, d) {
	// Programmeer hier je oplossing

	// Om de tests goed te laten verlopen moet dit blijven staan:
	return Array(a,b,c,d);
}
