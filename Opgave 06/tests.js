var tests = [
//  [  a[], exp, name]

	[ [7, 9, 5, 3, 0, 4], 0, 'Gewone array'],
	[ [], 0, 'Lege Array' ],
	[ [1], 1, 'Array met één element' ],
	[ [0, 3, 5, 2], 0, 'Eerste element is de kleinste' ],
	[ [3, 8, 2, 1, 0], 0, 'Laatste element is de kleinste' ],
	[ [5, -3, 2, 8], -3, 'Met negatief getal' ],
	[ [-2, 9, 5, -3], -3, 'Met twee negatieve getallen' ],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	exp = testdata[1];
	name = testdata[2];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a);		
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result == exp) {
		return false;
	}	

	// Create an alert div with the result 
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	// Create an alert div with the result 
	var resultTxt = "functionUnderTest(" + a + ") -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}
