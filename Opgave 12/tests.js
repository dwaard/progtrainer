var tests = [
//  [ exp, name]
	[  'Hello World!', 3],
	[  '', 0],
	[  'Wie dit leest is gek', 7],
	[  'aeiou', 5],
	[  'Lorem Ipsum', 4],
	[  'AEIOU', 5],
	[  'HBO-ICT Rocks', 3],
	[  'TEkst met eEn aantal KlInkers op willekeurige plaatsen in hOofdLETters in woOrden', 28],
	[  'Dit is een test om te kijken of er ook goed klinkers getest kunnen worden in, zoals veel mensen zouden zeggen, vrij lange zinnen waar heel veel woorden en vooral ook klinkers staan die uiteindelijk door jou goed geteld moeten worden', 80],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	exp = testdata[1];
	name = '[lege string]';
	if (a.length > 0)
		name = a;
	name += " heeft " + exp + " klinkers.";
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a);
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result===exp) {
		return false;
	}

	// Create an alert div with the result
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	if (!((typeof result)=='number')) {
		appendAlert('danger', result, 'De waarde in het return statement is geen getal, maar een ' + (typeof result));
		return true;
	}

	if (result < 0) {
		appendAlert('danger', result, 'Een negatief aantal klinkers is niet mogelijk!');
		return true;
	}

	var resultTxt = "functionUnderTest() -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}


function appendAlert(type, strong, msg) {
	var resultTxt = '<div class="alert alert-' + type + '" role="alert"><strong>' + strong + "</strong> " + msg + '</div>';
	$('#result').append(resultTxt);
}
