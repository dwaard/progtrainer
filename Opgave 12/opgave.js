/*
 * Maak deze functie zo dat het aantal klinkers (a, e, i, o, u) die in de tekst a wordt teruggegeven.
 * De parameter a bevat dus altijd een tekst. De functie telt dus het aantal keer dat een klinker
 * in die tekst voorkomt. Bij voorbeeld als a="Hallo Wereld!", dan is het antwoord 4.
 * Let op: ook de HOOFDLETTERS tellen hier mee als klinkers
 */
function functionUnderTest(a) {
	// Programmeer hier je oplossing. Gebruik het return statement om iets 'terug te geven'.

	return ;
}
