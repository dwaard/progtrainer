var tests = [
//  [  a, b, c, exp, name]
	[  1, 2, 3, 1  , 'a is the smallest'],
	[  2, 1, 3, 1  , 'b is the smallest'],
	[  2, 3, 1, 1  , 'c is the smallest'],
	[  1, 2, 2, 1  , 'a is the smallest, b and c are equal'],
	[  1, 2, 1, 1  , 'a is the smallest, a and c are equal'],
	[  1, 1, 2, 1  , 'a is the smallest, a and b are equal'],
	[  2, 1, 2, 1  , 'b is the smallest, a and c are equal'],
	[  2, 1, 1, 1  , 'b is the smallest, b and c are equal'],
	[  1, 1, 2, 1  , 'b is the smallest, a and b are equal'],
	[  2, 2, 1, 1  , 'c is the smallest, a and b are equal'],
	[  2, 1, 1, 1  , 'c is the smallest, c and b are equal'],
	[  1, 2, 1, 1  , 'c is the smallest, a and c are equal'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	a = testdata[0];
	b = testdata[1];
	c = testdata[2];
	exp = testdata[3];
	name = testdata[4];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(a, b, c);		
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result == exp) {
		return false;
	}
		
	// Create an alert div with the result 
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	// Create an alert div with the result 
	var resultTxt = "functionUnderTest(" + a + ", " + b + ", " + c + ") -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}


