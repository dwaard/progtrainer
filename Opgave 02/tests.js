var tests = [
//  [ v1,    v2,  exp, name]
	[  1,     1,    1, 'Test gelijke waarden'],
	[ 10,     4,   10, 'Test eerste getal grootste'],
	[  0,     3,    3, 'Test tweede getal grootste'],
	[ -5,    -7,   -5, 'Test negatieve getallen, eerste is de hoogste'],
	[ -8,    -4,   -4, 'Test negatieve getallen, tweede is de hoogste'],
	[ -7,     9,    9, 'Test positieve en negatieve getallen'],
	[ 'A',  'B',  'B', 'Test text'],
	[null, null, null, 'Test nulls'],
	[null,    3,    3, 'Test nulls'],
	[   2, null,    2, 'Test nulls'],
	[  "",   "",   "", 'Test nulls'],
	[  "",    3,    3, 'Test nulls'],
	[   2,   "",    2, 'Test nulls'],
];


/*
 * Calls the function under test and process the result
 */
function test(testdata) {
	v1 = testdata[0];
	v2 = testdata[1];
	exp = testdata[2];
	name = testdata[3];
	console.log("Testing: " + name);
	// Call the function under test and catch all the errors
	try{
		var result = functionUnderTest(v1, v2);		
	} catch(err) {
		appendAlert('danger', 'FOUTMELDING:', err);
		return true;
	}

	// return TRUE when result equals expected
	if (result==exp) {
		return false;
	}
		
	// Create an alert div with the result 
	if (((typeof result)=='undefined')) {
		appendAlert('danger', result, 'Er is geen returnwaarde. Typ iets achter het woord "return" in de opgave.');
		return true;
	}

	// Create an alert div with the result 
	var resultTxt = "functionUnderTest(" + v1 + ", " + v2 + ") -> " + result + " (verwacht resultaat: " + exp + ")";
	appendAlert('danger', name, resultTxt);
	return true;
}


function appendAlert(type, strong, msg) {
	var resultTxt = '<div class="alert alert-' + type + '" role="alert"><strong>' + strong + "</strong> " + msg + '</div>';
	$('#result').append(resultTxt);	
}